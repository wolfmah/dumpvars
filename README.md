# Ansible Role: dumpvars

Create a file on the target system containing the relevant Ansible variables available to that host. Useful for debugging.

This role is not idempotent; it will overwrite the file if it already exists. To mitigate this behavior, you should tag this role with `never` and `debug`.

## Requirements

None.

## Role variables

Available variables are listed below, along with default values:

    dumpvars_filepath: /tmp/ansible-vars

Allows to set a different filepath where the Ansible vars will be dumped.

## Role tags

None

## Dependencies

None.

## Examples

### Requirements

    - name: wolfmah.dumpvars
      src: git@gitlab.com:home.lan/infra-tools/ansible-roles/dumpvars.git
      scm: git
      version: master

### Playbooks

    - hosts: servers
      roles:
        - role: wolfmah.dumpvars
          vars:
            - dumpvars_filepath: /tmp/ansible-vars
          tags:
            - never
            - debug

## Ownership and License

The contributors are listed in [AUTHORS](AUTHORS).

This project uses the Mozilla Public License Version 2.0 (MPLv2) license, see [LICENSE](LICENSE).

To report an issue, use the GitLabs's project [issue tracker](https://gitlab.com/home.lan/infra-tools/ansible-roles/dumpvars/issues).
